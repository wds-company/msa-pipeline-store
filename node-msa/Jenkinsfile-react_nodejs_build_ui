#!/usr/bin/env groovy

def deployConfig
def bindingVarArray

def notifyLINE(token, status) {
    //def token = "hgSWq03t4oDr9R2leT9ObJ4ctiegicSkkYrxU8dWXF5" // ake
    //def token = "uNt7yepHIi55Xx7TUeMYKnhhI8p7eMGy9mdWmMGKDBG" // mon
    def jobName = env.JOB_NAME +' '+env.BRANCH_NAME
    def buildNo = env.BUILD_NUMBER
      
    def url = 'https://notify-api.line.me/api/notify'
    def message = "${jobName} Build #${buildNo} ${status} \r\n"
    sh "curl ${url} -H 'Authorization: Bearer ${token}' -F 'message=${message}'"
}

pipeline {
    agent any
    options {
        // using the Timestamper plugin we can add timestamps to the console log
        timestamps()
        // And we'd really like to be sure that this build doesn't hang forever, so
        // let's time it out after an hour.
        timeout(time: 30, unit: 'MINUTES')
    }
    parameters {
        string(name: 'MODULE_CONFIG', defaultValue: 'msa-react-emulator', description: 'Module Configuration Ex. training-msa-go-tong')
    }
    stages {
        stage('Load Configuration') {
            steps {
                script {
                    def moduleConfig = "${params['MODULE_CONFIG']}"
                    def module = load "${env.WORKSPACE}/groovy/load_configuration.groovy"
                    deployConfig = module.execute(moduleConfig)
                    bindingVarArray = deployConfig['binding_var']
                }
            }
        }
        stage('Create Directory') {
            steps {
                echo "Home workspace path : ${env.WORKSPACE}"
                sh "rm -rf ${env.WORKSPACE}/frontend"                  
                sh "mkdir -p ${env.WORKSPACE}/frontend"                       
            }
        }
        stage('Get Source') { // Get code
            steps {
                sh "git clone -b feature/develop ssh://git@bitbucket.beebuddy.net:7999/ssquare-apig/emulator-ui.git ${env.WORKSPACE}/frontend"
            }
        }
        stage('Binding Variable') {
            steps {
                script {
                    def module = load "${env.WORKSPACE}/groovy/binding_variable.groovy"
                    module.execute(bindingVarArray)
                }
            }
        }
        stage('Stop Existing Container') {
            steps {
                sh "docker ps -f name=${containerName} -q | xargs --no-run-if-empty docker container stop"
            }
        }
        stage('Remove Existing Container') {
            steps {
                sh "docker container ls -a -fname=${containerName} -q | xargs -r docker container rm"
            }
        }
        stage('Build Remove Existing Image') {
            steps {
                sh """
                    docker image ls \
                        | awk '{ print \$1,\$2,\$3 }' \
                        | grep ${imageName} \
                        | awk '{print \$3 }' \
                        | xargs -I {} docker image rm {}
                    """
            }
        }
        stage('Build Docker Image') {
            steps {
                sh "docker build -f ${dockerInputFile} -t ${imageName} ."
            }
        }
        stage('Deploy Container') {
            steps {
                sh "export DOCKER_HOST=unix:///var/run/docker.sock"
                sh "docker-compose -f ./composes/node/${dockerComposeFile} up -d"
            }
        }
    }

    //post {
    //    always {
    //        FREE_MEM = sh (script: 'free -m', returnStdout: true).trim()
    //        echo "Free memory: ${FREE_MEM}"
    //    }
    //}
    post{
        success{
            notifyLINE("hgSWq03t4oDr9R2leT9ObJ4ctiegicSkkYrxU8dWXF5", "succeed")
            notifyLINE("uNt7yepHIi55Xx7TUeMYKnhhI8p7eMGy9mdWmMGKDBG", "succeed")
        }
        failure{
            notifyLINE("hgSWq03t4oDr9R2leT9ObJ4ctiegicSkkYrxU8dWXF5", "failed")
            notifyLINE("uNt7yepHIi55Xx7TUeMYKnhhI8p7eMGy9mdWmMGKDBG", "failed")
        }
    }
}
